# CLEF2024--CheckThat-Lab
This repository contains the _dataset_, _format checker, scorer and baselines_ for each task of the [CLEF2024-CheckThat! Lab](https://checkthat.gitlab.io/).

The **2024 edition** of the lab zooms into some of the problems and —- for the first time -— we will offer **six tasks:**
Task 1 asks to determine whether a text is check-worthy; Task 2 requires to assess whether a text snippet is subjective or not; Task 3 looks for identifying the persuasion techniques; Task 4 requires to detect hero, villain, and victim from memes; Task 5 intends to identify authorities that should be trusted to verify a contended claim; and Task 6 aims to detect robustness of credibility assessment with adversarial examples.

  - [Task 1: Check-worthiness estimation](task1)
      - **Arabic**
      - **English**
      - **Spanish**
  - [Task 2: Subjectivity in news articles](task2)
      - **Arabic**      
      - **English**
      - **German**
      - **Italian**
      - **Multilingual**
  - [Task 3: Detection of Persuasion Techniques in News Articles](https://propaganda.math.unipd.it/checkthat24_persuasion/)
    - **Arabic**
    - **Bulgarian**
    - **English**
    - **Portuguese**
    - **Slovene**    	
  - [Task 4: Detecting Hero, Villain, and Victim from Memes](task4)
      - **Arabic**
      - **English**
      - **Code-mixing of Hindi and English**
  - [Task 5: Authority Finding in Twitter](task5)
      - **Arabic**
      - **English**
  - [Task 6: Robustness of Credibility Assessment with Adversarial Examples](task6)
      - **English**


# Licensing

Please check the task-specific directory for the licensing of the respective dataset.

# Credits

**Lab Organizers:** Please find on the task website: https://checkthat.gitlab.io/

# Contact
Please join on Slack channel for any queries: [https://join.slack.com/t/ct-participants/shared_invite/zt-1me6ywqxu-0Q~r7vCm6gfmrmN9xuOO7A](https://join.slack.com/t/ct-participants/shared_invite/zt-1me6ywqxu-0Q~r7vCm6gfmrmN9xuOO7A)


Or send an email to: clef-factcheck@googlegroups.com


# Citation

Please find relevant papers below:

```
@InProceedings{clef-checkthat:2023,
	author = "Barr{\'o}n-Cede{\~{n}}o, Alberto
		and Alam, Firoj
		and Galassi, Andrea
        and {Da San Martino}, Giovanni
        and Nakov, Preslav and
        and Elsayed, Tamer
        and Azizov, Dilshod
        and Caselli, Tommaso
		and Cheema, {Gullal S.}
        and Haouari, Fatima
		and Hasanain, Maram
        and Kutlu, Mucahid
        and Li, Chengkai
        and Ruggeri, Federico
		and Struß, Julia Maria
		and Zaghouani, Wajdi",
	title = "Overview of the {CLEF}--2023 {CheckThat! Lab} Checkworthiness, Subjectivity, Political Bias, Factuality, and Authority of News Articles and Their Source",
	editor = "Arampatzis, Avi
		and Kanoulas, Evangelos
		and Tsikrika, Theodora
		and Vrochidis, Stefanos
		and Giachanou, Anastasia
		and Li, Dan
		and Aliannejadi, Mohammad
		and Vlachos, Michalis
		and Faggioli, Guglielmo
		and Ferro, Nicola",
	booktitle = "Experimental IR Meets Multilinguality, Multimodality, and Interaction. Proceedings of the Fourteenth International Conference of the CLEF Association
({CLEF} 2023)",
year="2023",
}


@InProceedings{10.1007/978-3-031-28241-6_59,
	author="Barr{\'o}n-Cede{\~{n}}o, Alberto
		and Alam, Firoj
		and Caselli, Tommaso
		and Da San Martino, Giovanni
		and Elsayed, Tamer
		and Galassi, Andrea
		and Haouari, Fatima
		and Ruggeri, Federico
		and Stru{\ss}, Julia Maria
		and Nandi, Rabindra Nath
		and Cheema, Gullal S.
		and Azizov, Dilshod
		and Nakov, Preslav",
	title="The {CLEF}-2023 {CheckThat! Lab}: Checkworthiness, Subjectivity, Political Bias, Factuality, and Authority",
	editor="Kamps, Jaap
		and Goeuriot, Lorraine
		and Crestani, Fabio
		and Maistro, Maria
		and Joho, Hideo
		and Davis, Brian
		and Gurrin, Cathal
		and Kruschwitz, Udo
		and Caputo, Annalina",		
	booktitle="Advances in Information Retrieval",
	year="2023",
	publisher="Springer Nature Switzerland",
	address="Cham",
	pages="506--517",
	isbn="978-3-031-28241-6"
}

@InProceedings{CheckThat:ECIR2022,
author="Nakov, Preslav and Barr{\'o}n-Cede{\~{n}}o, Alberto and Da San Martino, Giovanni and Alam, Firoj and Stru{\ss}, Julia Maria and Mandl, Thomas and M{\'i}guez, Rub{\'e}n and Caselli, Tommaso and Kutlu, Mucahid and Zaghouani, Wajdi and Li, Chengkai and Shaar, Shaden and Shahi, Gautam Kishore and Mubarak, Hamdy and Nikolov, Alex and Babulkov, Nikolay and Kartal, Yavuz Selim and Beltr{\'a}n, Javier",
editor="Hagen, Matthias and Verberne, Suzan and Macdonald, Craig and Seifert, Christin and Balog, Krisztian and N{\o}rv{\aa}g, Kjetil and Setty, Vinay",
title="The CLEF-2022 CheckThat! Lab on Fighting the COVID-19 Infodemic and Fake News Detection",
booktitle="Advances in Information Retrieval",
year="2022",
publisher="Springer International Publishing",
address="Cham",
pages="416--428",
isbn="978-3-030-99739-7"
}

@InProceedings{clef-checkthat:2022:LNCS,
author = {Nakov, Preslav and Barr\'{o}n-Cede\~{n}o, Alberto and Da San Martino, Giovanni and Alam, Firoj and Stru\ss{}, Julia Maria and Mandl, Thomas and M\'{\i}guez, Rub\'{e}n and Caselli, Tommaso and Kutlu, Mucahid and Zaghouani, Wajdi and Li, Chengkai and Shaar, Shaden and Shahi, Gautam Kishore and Mubarak, Hamdy and Nikolov, Alex and Babulkov, Nikolay and Kartal, Yavuz Selim and Beltr\'{a}n, Javier and Wiegand, Michael and Siegel, Melanie and Köhler, Juliane},
title = "Overview of the {CLEF}-2022 {CheckThat}! Lab on Fighting the {COVID-19} Infodemic and Fake News Detection",
year = {2022},
booktitle = "Proceedings of the 13th International Conference of the CLEF Association: Information Access Evaluation meets Multilinguality, Multimodality, and Visualization",
series = {CLEF~'2022},
address = {Bologna, Italy},
}



@InProceedings{clef-checkthat:2022:task1,
author = {Nakov, Preslav and Barr\'{o}n-Cede\~{n}o, Alberto and Da San Martino, Giovanni and Alam, Firoj and M\'{\i}guez, Rub\'{e}n and Caselli, Tommaso and Kutlu, Mucahid and Zaghouani, Wajdi and Li, Chengkai and Shaar, Shaden and Mubarak, Hamdy and Nikolov, Alex and Kartal, Yavuz Selim and Beltr\'{a}n, Javier},
title = "Overview of the {CLEF}-2022 {CheckThat}! Lab Task 1 on Identifying Relevant Claims in Tweets",
year = {2022},
booktitle = "Working Notes of CLEF 2022---Conference and Labs of the Evaluation Forum",
series = {CLEF~'2022},
address = {Bologna, Italy},
}


@InProceedings{clef-checkthat:2022:task2,
author = {Nakov, Preslav and Da San Martino, Giovanni and Alam, Firoj and Shaar, Shaden and Mubarak, Hamdy and Babulkov, Nikolay},
title = "Overview of the {CLEF}-2022 {CheckThat}! Lab Task 2 on Detecting Previously Fact-Checked Claims",
year = {2022},
booktitle = "Working Notes of CLEF 2022---Conference and Labs of the Evaluation Forum",
series = {CLEF~'2022},
address = {Bologna, Italy},
}

```

## Additional Resources (Tools/Source code)
We listed the following tools/source code, which might be helpful to run the experiments.
* https://fasttext.cc/docs/en/supervised-tutorial.html
* https://huggingface.co/docs/transformers/training
* https://github.com/Tiiiger/bert_score
* https://github.com/clef2018-factchecking/clef2018-factchecking
* https://github.com/utahnlp/x-fact
* https://github.com/firojalam/COVID-19-disinformation/tree/master/bin
* https://github.com/facebookresearch/mmf

## Recommended reading
The following papers might be useful. We have not provided exhaustive list. But these could be a good start.<br>
[Download bibliography](bibtex/bibliography.bib)

**Multimodal**
* Gullal Singh Cheema, Sherzod Hakimov, Abdul Sittar, Eric Müller-Budack, Christian Otto, and Ralph Ewerth. 2022. **[MM-Claims: A Dataset for Multimodal Claim Detection in Social Media](https://aclanthology.org/2022.findings-naacl.72/)**. In Findings of the Association for Computational Linguistics: NAACL 2022, pages 962–979, Seattle, United States. Association for Computational Linguistics.
* Firoj Alam, Stefano Cresci, Tanmoy Chakraborty, Fabrizio Silvestri, Dimiter Dimitrov, Giovanni Da San Martino, Shaden Shaar, Hamed Firooz, and Preslav Nakov. 2022. **[A Survey on Multimodal Disinformation Detection](https://aclanthology.org/2022.coling-1.576/)**. In Proceedings of the 29th International Conference on Computational Linguistics, pages 6625–6643, Gyeongju, Republic of Korea. International Committee on Computational Linguistics.
* Gullal Singh Cheema, Sherzod Hakimov, Eric Müller-Budack, and Ralph Ewerth. 2021. **[On the Role of Images for Analyzing Claims in Social Media](https://ceur-ws.org/Vol-2829/paper3.pdf)**. In Proceedings of the 2nd International Workshop on Cross-lingual Event-centric Open Analytics co-located with the 30th The Web Conference (WWW 2021).
* Dimitrina Zlatkova, Preslav Nakov, and Ivan Koychev. 2019. **[Fact-Checking Meets Fauxtography: Verifying Claims About Images](https://aclanthology.org/D19-1216/)**. In Proceedings of the 2019 Conference on Empirical Methods in Natural Language Processing and the 9th International Joint Conference on Natural Language Processing (EMNLP-IJCNLP), pages 2099–2108, Hong Kong, China. Association for Computational Linguistics.


**Fact-checking**
* Nakov, Preslav, David Corney, Maram Hasanain, Firoj Alam, and Tamer Elsayed. **["Automated Fact-Checking for Assisting Human Fact-Checkers."](https://www.ijcai.org/proceedings/2021/0619.pdf)** in IJCAI, 2021.
* Shaar, Shaden, Firoj Alam, Giovanni Da San Martino, and Preslav Nakov. **"Assisting the Human Fact-Checkers: Detecting All Previously Fact-Checked Claims in a Document."** arXiv preprint arXiv:2109.07410 (2021).
* Shaar, Shaden, Firoj Alam, Giovanni Da San Martino, and Preslav Nakov. **"The role of context in detecting previously fact-checked claims."** arXiv preprint arXiv:2104.07423 (2021).

**COVID-19 Infodemic**
* Alam, Firoj, Shaden Shaar, Fahim Dalvi, Hassan Sajjad, Alex Nikolov, Hamdy Mubarak, Giovanni Da San Martino et al. **["Fighting the COVID-19 Infodemic: Modeling the Perspective of Journalists, Fact-Checkers, Social Media Platforms, Policy Makers, and the Society."](https://aclanthology.org/2021.findings-emnlp.56.pdf)** In Findings of the Association for Computational Linguistics: EMNLP 2021, pp. 611-649. 2021.
* Shaar, Shaden, Firoj Alam, Giovanni Da San Martino, Alex Nikolov, Wajdi Zaghouani, Preslav Nakov, and Anna Feldman. **"Findings of the NLP4IF-2021 Shared Tasks on Fighting the COVID-19 Infodemic and Censorship Detection."** In Proceedings of the Fourth Workshop on NLP for Internet Freedom: Censorship, Disinformation, and Propaganda, pp. 82-92. 2021.
* Nakov, Preslav, Firoj Alam, Shaden Shaar, Giovanni Da San Martino, and Yifan Zhang. **"A Second Pandemic? Analysis of Fake News about COVID-19 Vaccines in Qatar."** In Proceedings of the International Conference on Recent Advances in Natural Language Processing (RANLP 2021), pp. 1010-1021. 2021.
* Nakov, Preslav, Firoj Alam, Shaden Shaar, Giovanni Da San Martino, and Yifan Zhang. **"COVID-19 in Bulgarian social media: Factuality, harmfulness, propaganda, and framing."** In Proceedings of the International Conference on Recent Advances in Natural Language Processing (RANLP 2021), pp. 997-1009. 2021.

**Tasks papers from previous years**
* Nakov, Preslav, Giovanni Da San Martino, Tamer Elsayed, Alberto Barrón-Cedeño, Rubén Míguez, Shaden Shaar, Firoj Alam et al. **"Overview of the CLEF–2021 CheckThat! Lab on Detecting Check-Worthy Claims, Previously Fact-Checked Claims, and Fake News."** In International Conference of the Cross-Language Evaluation Forum for European Languages, pp. 264-291. Springer, Cham, 2021.
* Shaar, Shaden, Maram Hasanain, Bayan Hamdan, Zien Sheikh Ali, Fatima Haouari, Alex Nikolov, Mücahid Kutlu et al. **"Overview of the CLEF-2021 CheckThat! lab task 1 on check-worthiness estimation in tweets and political debates."** In CLEF (Working Notes). 2021.
* Shahi, Gautam Kishore, Julia Maria Struß, and Thomas Mandl. **"Overview of the CLEF-2021 CheckThat! lab task 3 on fake news detection."** Working Notes of CLEF (2021).
* Shaar, Shaden, Fatima Haouari, Watheq Mansour, Maram Hasanain, Nikolay Babulkov, Firoj Alam, Giovanni Da San Martino, Tamer Elsayed, and Preslav Nakov. **"Overview of the CLEF-2021 CheckThat! lab task 2 on detecting previously fact-checked claims in tweets and political debates."** In CLEF (Working Notes). 2021.
* Barrón-Cedeño, Alberto, Tamer Elsayed, Preslav Nakov, Giovanni Da San Martino, Maram Hasanain, Reem Suwaileh, Fatima Haouari et al. **"Overview of CheckThat! 2020: Automatic identification and verification of claims in social media."** In International Conference of the Cross-Language Evaluation Forum for European Languages, pp. 215-236. Springer, Cham, 2020.
* Shaar, Shaden, Alex Nikolov, Nikolay Babulkov, Firoj Alam, Alberto Barrón-Cedeno, Tamer Elsayed, Maram Hasanain et al. **"Overview of CheckThat! 2020 English: Automatic identification and verification of claims in social media."** In International Conference of the Cross-Language Evaluation Forum for European Languages. 2020.
* Hasanain, Maram, Fatima Haouari, Reem Suwaileh, Zien Sheikh Ali, Bayan Hamdan, Tamer Elsayed, Alberto Barrón-Cedeno, Giovanni Da San Martino, and Preslav Nakov. **"Overview of CheckThat! 2020 Arabic: Automatic identification and verification of claims in social media."** In International Conference of the Cross-Language Evaluation Forum for European Languages. 2020.
* Elsayed, Tamer, Preslav Nakov, Alberto Barrón-Cedeno, Maram Hasanain, Reem Suwaileh, Giovanni Da San Martino, and Pepa Atanasova. **"Overview of the CLEF-2019 CheckThat! Lab: automatic identification and verification of claims."** In International Conference of the Cross-Language Evaluation Forum for European Languages, pp. 301-321. Springer, Cham, 2019.
* Elsayed, Tamer, Preslav Nakov, Alberto Barrón-Cedeno, Maram Hasanain, Reem Suwaileh, Giovanni Da San Martino, and Pepa Atanasova. **"CheckThat! at CLEF 2019: Automatic identification and verification of claims."** In European Conference on Information Retrieval, pp. 309-315. Springer, Cham, 2019.
* Atanasova, Pepa, Preslav Nakov, Georgi Karadzhov, Mitra Mohtarami, and Giovanni Da San Martino. **"Overview of the CLEF-2019 CheckThat! Lab: Automatic Identification and Verification of Claims. Task 1: Check-Worthiness."** CLEF (Working Notes) 2380 (2019).
* Nakov, Preslav, Alberto Barrón-Cedeno, Tamer Elsayed, Reem Suwaileh, Lluís Màrquez, Wajdi Zaghouani, Pepa Atanasova, Spas Kyuchukov, and Giovanni Da San Martino. **"Overview of the CLEF-2018 CheckThat! Lab on automatic identification and verification of political claims."** In International conference of the cross-language evaluation forum for european languages, pp. 372-387. Springer, Cham, 2018.
* Barrón-Cedeno, Alberto, Tamer Elsayed, Reem Suwaileh, Lluís Màrquez, Pepa Atanasova, Wajdi Zaghouani, Spas Kyuchukov, Giovanni Da San Martino, and Preslav Nakov. **"Overview of the CLEF-2018 CheckThat! Lab on Automatic Identification and Verification of Political Claims. Task 2: Factuality."**   CLEF (Working Notes) 2125 (2018).
* Atanasova, Pepa, Alberto Barron-Cedeno, Tamer Elsayed, Reem Suwaileh, Wajdi Zaghouani, Spas Kyuchukov, Giovanni Da San Martino, and Preslav Nakov. **"Overview of the CLEF-2018 CheckThat! lab on automatic identification and verification of political claims. Task 1: Check-worthiness."** arXiv preprint arXiv:1808.05542 (2018).
